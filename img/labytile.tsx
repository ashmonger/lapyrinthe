<?xml version="1.0" encoding="UTF-8"?>
<tileset name="labytile" tilewidth="32" tileheight="32" tilecount="24" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="32" height="32" source="player/p_down.png"/>
 </tile>
 <tile id="1">
  <image width="32" height="32" source="player/p_left.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="32" source="player/p_right.png"/>
 </tile>
 <tile id="3">
  <image width="32" height="32" source="player/p_up.png"/>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="items/carrot.png"/>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="items/flag.png"/>
 </tile>
 <tile id="6">
  <image width="32" height="32" source="items/radis.png"/>
 </tile>
 <tile id="7">
  <image width="32" height="32" source="items/salad.png"/>
 </tile>
 <tile id="8">
  <image width="32" height="32" source="path/0000.png"/>
 </tile>
 <tile id="9">
  <image width="32" height="32" source="path/0001.png"/>
 </tile>
 <tile id="10">
  <image width="32" height="32" source="path/0010.png"/>
 </tile>
 <tile id="11">
  <image width="32" height="32" source="path/0011.png"/>
 </tile>
 <tile id="12">
  <image width="32" height="32" source="path/0100.png"/>
 </tile>
 <tile id="13">
  <image width="32" height="32" source="path/0101.png"/>
 </tile>
 <tile id="14">
  <image width="32" height="32" source="path/0110.png"/>
 </tile>
 <tile id="15">
  <image width="32" height="32" source="path/0111.png"/>
 </tile>
 <tile id="16">
  <image width="32" height="32" source="path/1000.png"/>
 </tile>
 <tile id="17">
  <image width="32" height="32" source="path/1001.png"/>
 </tile>
 <tile id="18">
  <image width="32" height="32" source="path/1010.png"/>
 </tile>
 <tile id="19">
  <image width="32" height="32" source="path/1011.png"/>
 </tile>
 <tile id="20">
  <image width="32" height="32" source="path/1100.png"/>
 </tile>
 <tile id="21">
  <image width="32" height="32" source="path/1101.png"/>
 </tile>
 <tile id="22">
  <image width="32" height="32" source="path/1110.png"/>
 </tile>
 <tile id="23">
  <image width="32" height="32" source="path/1111.png"/>
 </tile>
</tileset>
