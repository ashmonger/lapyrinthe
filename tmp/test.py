#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame as pg

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)

pg.init()

if (19201, 1080) in pg.display.list_modes():
    SCREEN_DEF = (1920, 1080)
    FENETRE = pg.display.set_mode(
        SCREEN_DEF,
        pg.HWSURFACE | pg.DOUBLEBUF | pg.FULLSCREEN
    )
elif (1366, 768) in pg.display.list_modes():
    SCREEN_DEF = (1366, 768)
    FENETRE = pg.display.set_mode(
        SCREEN_DEF,
        pg.HWSURFACE | pg.DOUBLEBUF | pg.FULLSCREEN
    )
else:
    SCREEN_DEF = (800, 600)
    FENETRE = pg.display.set_mode(SCREEN_DEF)


class Game():
    def __init__(self, x, y, color):
        GAME_SIZE = (x, y)
        self.image = pg.Surface(GAME_SIZE)
        self.image.fill(color)
        self.GAME_CENTER = (
            (SCREEN_DEF[0] - GAME_SIZE[0]) / 2,
            (SCREEN_DEF[1] - GAME_SIZE[1]) / 2
        )


CONTINUER = 1
while CONTINUER:
    FENETRE.fill(YELLOW)
    pg.display.flip()
    MENU = 1
    GAME = 0

    # Menu du jeu
    while MENU:
        for event in pg.event.get():
            if (
                event.type == pg.QUIT or
                event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE
            ):
                CONTINUER = 0
                MENU = 0
                GAME = 0
            if event.type == pg.KEYDOWN and event.key == pg.K_RETURN:
                MENU = 0
                GAME = 1
                FENETRE.fill(BLUE)
        pg.display.flip()

    while GAME:
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    MENU = 1
                    GAME = 0
                if event.key == pg.K_F1:
                    F1 = Game(200, 200, GREEN)
                    FENETRE.fill(RED)
                    FENETRE.blit(F1.image, F1.GAME_CENTER)
                if event.key == pg.K_F2:
                    F2 = Game(300, 100, BLUE)
                    FENETRE.fill(RED)
                    FENETRE.blit(F2.image, F2.GAME_CENTER)
                if event.key == pg.K_F3:
                    F3 = Game(100, 300, BLACK)
                    FENETRE.fill(RED)
                    FENETRE.blit(F3.image, F3.GAME_CENTER)
        pg.display.flip()

pg.quit()
