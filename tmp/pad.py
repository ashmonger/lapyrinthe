import pygame as pg

# * A = 0
# * B = 1
# * X = 2
# * Y = 3
# * LB = 4
# * RB = 5
# * start = 6
# * back = 7

pg.init()

screen = pg.display.set_mode((300, 300))

pad = pg.joystick.Joystick(0)
pad.init()

def deadzone(value):
    dz = 0.15
    if value > dz:
        return (value - dz) / (1 - dz)
    elif value < -dz:
        return (value + dz) / (1 - dz)
    else:
        return 0

continuer = True
while continuer:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            continuer = False
        if event.type == pg.JOYBUTTONDOWN:
            print(event.button)
        if event.type == pg.JOYAXISMOTION:
            # print(event.axis, event.value)
            if event.axis == 0 and event.value > 0.5:
                print("LSTICK-droite")
            if event.axis == 0 and event.value < -0.5:
                print("LSTICK-gauche")
            if event.axis == 1 and event.value > 0.5:
                print("LSTICK-bas")
            if event.axis == 1 and event.value < -0.5:
                print("LSTICK-haut")
            if event.axis == 3 and event.value > 0.5:
                print("RSTICK-droite")
            if event.axis == 3 and event.value < -0.5:
                print("RSTICK-gauche")
            if event.axis == 4 and event.value > 0.5:
                print("RSTICK-bas")
            if event.axis == 4 and event.value < -0.5:
                print("RSTICK-haut")

pg.quit()
