#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Lapyrinthe
main.py - Code de base pour lancer l'UI
lab.py - Classes pour la création du Labyrinthe
player.py - Classes pour le joueur
constants.py - Fichier de constantes
img/*.png - cases du Labyrinthe
"""

import random
import pygame as pg
import constants as PC
import sys

if getattr(sys, 'frozen', False):
    basedir = sys._MEIPASS
else:
    basedir = "."


class Cell():
    """ Chaque cellule du Labyrinthe """

    def __init__(self, x, y):
        """ x, y are coordinates
            visited is the status while making the Laby
            doors are open(1), close(0), [n, s, e, w]
            tag is the Nth of each cell
        """
        self.x = x
        self.y = y
        self.doors = [0, 0, 0, 0]
        self.tag = int()
        self.num = int()


class Laby():
    """ Creation et affichage du Labyrinthe
        methode de la fusion aleatoire des chemins
    """

    def __init__(self, PL):
        """ Initialise la liste des cellules """
        self.cell_list = list()
        self.celltag = list()
        self.neigh = list()
        self.ctag = int()
        self.ntag = int()
        number = 0
        for i in range(0, PL.COTE_Y):
            for j in range(0, PL.COTE_X):
                cell = Cell(j, i)
                cell.num = number
                cell.tag = number
                self.cell_list.append(cell)
                number += 1

    def make_door(self, celllist, cell, neighbours):
        """ Permet l'ouverture de portes entre cases adjacentes:
        si les cases ont un mur commun, décider aléatoirement quel mur ouvrir,
        tant qu'elles n'ont pas le même tag
        """
        ng = random.choice(neighbours)
        self.ctag = cell.tag
        if ng != "X":
            self.ntag = ng.tag
            if (
                (cell.tag != ng.tag) and (cell.y == ng.y or cell.x == ng.x)
            ):
                # Ouverture porte Nord
                if cell.y - ng.y > 0:
                    cell.doors[0] = 1
                    ng.doors[1] = 1
                    ng.tag = cell.tag
                # Ouverture porte Sud
                if cell.y - ng.y < 0:
                    cell.doors[1] = 1
                    ng.doors[0] = 1
                    ng.tag = cell.tag
                # Ouverture porte Est
                if cell.x - ng.x < 0:
                    cell.doors[2] = 1
                    ng.doors[3] = 1
                    ng.tag = cell.tag
                # Ouverture porte Ouest
                if cell.x - ng.x > 0:
                    cell.doors[3] = 1
                    ng.doors[2] = 1
                    ng.tag = cell.tag
                for item in celllist:
                    if item.tag == self.ntag:
                        item.tag = self.ctag

    def make_lab(self, PL, celllist):
        """ faire le Labyrinthe """
        self.celltag = [0]
        while self.celltag.count(self.celltag[0]) < (PL.COTE_X * PL.COTE_Y):
            c = random.randrange(0, PL.COTE_X * PL.COTE_Y)
            self.celltag.clear()
            N = celllist[c].num
            # les cellules voisines sont stockées dans une liste
            # elle sont sous la forme [N, S, E, W].
            # Si il n'y a pas de voisin dans la direction, il y a un X
            # à la place de l'objet Cell prévu
            self.neigh = list()
            # Cellule Nord
            if celllist[c].y > 0 and celllist[c].y < PL.COTE_Y:
                self.neigh.append(celllist[N - PL.COTE_X])
            else:
                self.neigh.append("X")
            # Cellule Sud
            if celllist[c].y >= 0 and celllist[c].y < PL.COTE_Y - 1:
                self.neigh.append(celllist[N + PL.COTE_X])
            else:
                self.neigh.append("X")
            # Cellule Est
            if celllist[c].x >= 0 and celllist[c].x < PL.COTE_X - 1:
                self.neigh.append(celllist[N + 1])
            else:
                self.neigh.append("X")
            # Cellule Ouest
            if celllist[c].x > 0 and celllist[c].x < PL.COTE_X:
                self.neigh.append(celllist[N - 1])
            else:
                self.neigh.append("X")
            # self.liste.pop()
            self.make_door(celllist, celllist[c], self.neigh)
            for item in celllist:
                self.celltag.append(item.tag)

    @classmethod
    def display(self, PL, celllist):
        """ Afficher le Labyrinthe """
        for item in celllist:
            case = pg.image.load(
                "{}/img/path/{}{}{}{}.png".format(
                    basedir,
                    item.doors[0],
                    item.doors[1],
                    item.doors[2],
                    item.doors[3])
            ).convert_alpha()
            PL.SCREEN.blit(case, (item.x * PC.CELL_X, item.y * PC.CELL_Y))
            if item.num == 0:
                flag = PL.FLAG.convert_alpha()
                PL.SCREEN.blit(flag, (0, 0))
            if item.num == (PL.COTE_X * PL.COTE_Y - 1) and PL.CARROT_GOT == 0:
                carrot = PL.CARROT
                PL.SCREEN.blit(carrot, PL.CARROT_COORD)
            if item.num == (PL.COTE_X - 1) and PL.RADIS_GOT == 0:
                radis = PL.RADIS
                PL.SCREEN.blit(radis, PL.RADIS_COORD)
            if item.num == ((PL.COTE_X - 1) * PL.COTE_Y) and PL.SALAD_GOT == 0:
                salad = PL.SALAD
                PL.SCREEN.blit(salad, PL.SALAD_COORD)

    @classmethod
    def display_hud(self, PL):
        """ affichage du HUD """
        if PL.CARROT_GOT == 1:
            carrot = PL.CARROT
            PL.SCREEN.blit(carrot, PL.CARROT_HUD)
        if PL.RADIS_GOT == 1:
            radis = PL.RADIS
            PL.SCREEN.blit(radis, PL.RADIS_HUD)
        if PL.SALAD_GOT == 1:
            salad = PL.SALAD
            PL.SCREEN.blit(salad, PL.SALAD_HUD)
