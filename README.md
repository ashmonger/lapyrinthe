# lapyrinthe
Smol labyrinthe game in python and pygame, for Linux, MacOSX, and Windows

* [![Codacy Badge](https://api.codacy.com/project/badge/Grade/1fd9953c45474393b3e73f9918d98593)](https://www.codacy.com/app/ashmonger/lapyrinthe?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=ashmonger/lapyrinthe&amp;utm_campaign=Badge_Grade)
* [![Build status](https://ci.appveyor.com/api/projects/status/5cuuyh60kwo84035/branch/master?svg=true)](https://ci.appveyor.com/project/ashmonger/lapyrinthe/branch/master)
* [![pipeline status](https://gitlab.com/ashmonger/lapyrinthe/badges/master/pipeline.svg)](https://gitlab.com/ashmonger/lapyrinthe/commits/master)
* [![coverage report](https://gitlab.com/ashmonger/lapyrinthe/badges/master/coverage.svg)](https://gitlab.com/ashmonger/lapyrinthe/commits/master)

## How to play:
* _Start_ : hit Return
* _Move_ : arrow keys
* _Quit_ : Escape
* _Win_ : eat all the food!

### Credits
* The code and GFX are GPLv3
* The SFX are CC0
* The music is CCBY

#### code
* Ashmonger

#### GFX
* Ashmonger

#### SFX
* [Jalastram](https://opengameart.org/users/jalastram)
* [Kastenfrosch](https://freesound.org/people/Kastenfrosch/)

#### Music
* [Nicolai Heidlas](https://soundcloud.com/nicolai-heidlas) - [Take The Chance](https://cchound.com/post/173312518210/source-soundcloud-author-nicolai-heidlas-cc)
