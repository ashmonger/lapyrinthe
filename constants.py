#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Lapyrinthe
main.py - Code de base pour lancer l'UI
lab.py - Classes pour la création du Labyrinthe
player.py - Classes pour le joueur
constants.py - Fichier de constantes
img/*.png - cases du Labyrinthe
"""

import pygame as pg
import sys
import json


if getattr(sys, 'frozen', False):
    basedir = sys._MEIPASS
else:
    basedir = "."

# Mixer Pre-Init
# pygame.mixer.init(frequency=22050, size=-16, channels=2, buffer=512)
pg.mixer.pre_init(44100, -16, 2, 1024)
pg.mixer.init()

# Nombre et taille des cellules
CELL_X = 32  # taille X des cellules
CELL_Y = 32  # taille Y des cellules


class Splash():
    def __init__(self):
        self.FENETRE = pg.display.set_mode((800, 600), pg.VIDEORESIZE)
        self.ACCUEIL = pg.image.load("{}/img/splash.png".format(basedir))


ICONE = pg.image.load("{}/img/icon.png".format(basedir))

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Scores
try:
    json.load(open('scores.dat', 'r'))
except UnicodeDecodeError:
    l_scores = {'hard': 9999, 'easy': 9999, 'medium': 9999}
    json.dump(l_scores, open('scores.dat', 'w'))
    SCORES = json.load(open('scores.dat', 'r'))
except FileNotFoundError:
    l_scores = {'hard': 9999, 'easy': 9999, 'medium': 9999}
    json.dump(l_scores, open('scores.dat', 'w'))
    SCORES = json.load(open('scores.dat', 'r'))
else:
    SCORES = json.load(open('scores.dat', 'r'))

# Music
pg.mixer.music.load(
    "{}/sfx/Nicolai_Heidlas-Take_The_Chance.ogg".format(basedir)
)

# Items sounds
ITEM = pg.mixer.Sound("{}/sfx/item.ogg".format(basedir))

# Player sounds
JUMP = pg.mixer.Sound("{}/sfx/jump.ogg".format(basedir))
