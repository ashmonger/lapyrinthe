#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Lapyrinthe
main.py - Code de base pour lancer l'UI
lab.py - Classes pour la création du Labyrinthe
player.py - Classes pour le joueur
constants.py - Fichier de constantes
img/*.png - cases du Labyrinthe
"""

import constants as PC
import pygame as pg
import sys

if getattr(sys, 'frozen', False):
    basedir = sys._MEIPASS
else:
    basedir = "."


class Perso():
    """ Cree le personnage """

    def __init__(self, celllist):
        self.celllist = celllist
        self.cell = self.celllist[0]
        self.perso_pos = (self.cell.x, self.cell.y)
        # Player images
        self.P_UP = pg.image.load(
            "{}/img/player/p_up.png".format(basedir)
        ).convert_alpha()
        self.P_DOWN = pg.image.load(
            "{}/img/player/p_down.png".format(basedir)
        ).convert_alpha()
        self.P_LEFT = pg.image.load(
            "{}/img/player/p_left.png".format(basedir)
        ).convert_alpha()
        self.P_RIGHT = pg.image.load(
            "{}/img/player/p_right.png".format(basedir)
        ).convert_alpha()
        self.perso = self.P_RIGHT

    def collect(self, PL):
        """Get item"""
        if self.perso_pos == PL.CARROT_COORD:
            PL.CARROT_GOT = 1
            PC.ITEM.play()
        if self.perso_pos == PL.RADIS_COORD:
            PL.RADIS_GOT = 1
            PC.ITEM.play()
        if self.perso_pos == PL.SALAD_COORD:
            PL.SALAD_GOT = 1
            PC.ITEM.play()

    def move(self, PL, move):
        """ pour bouger le personnage en fonction
        de la case où il se trouve
        """
        if (
                move == "up" and
                self.cell.num - PL.COTE_X >= 0 and
                self.cell.doors[0] == 1
        ):
            PC.JUMP.play()
            self.cell = self.celllist[self.cell.num - PL.COTE_X]
            self.perso_pos = (self.cell.x * PC.CELL_X, self.cell.y * PC.CELL_Y)
            self.perso = self.P_UP
        elif (
                move == "down" and
                self.cell.num + PL.COTE_X <= (PL.COTE_X * PL.COTE_Y) - 1 and
                self.cell.doors[1] == 1
        ):
            PC.JUMP.play()
            self.cell = self.celllist[self.cell.num + PL.COTE_X]
            self.perso_pos = (self.cell.x * PC.CELL_X, self.cell.y * PC.CELL_Y)
            self.perso = self.P_DOWN
        elif (
                move == "left" and
                self.cell.num - 1 >= 0 and
                self.cell.doors[3] == 1
        ):
            PC.JUMP.play()
            self.cell = self.celllist[self.cell.num - 1]
            self.perso_pos = (self.cell.x * PC.CELL_X, self.cell.y * PC.CELL_Y)
            self.perso = self.P_LEFT
        elif (
                move == "right" and
                self.cell.num + 1 <= (PL.COTE_X * PL.COTE_Y) - 1 and
                self.cell.doors[2] == 1
        ):
            PC.JUMP.play()
            self.cell = self.celllist[self.cell.num + 1]
            self.perso_pos = (self.cell.x * PC.CELL_X, self.cell.y * PC.CELL_Y)
            self.perso = self.P_RIGHT
