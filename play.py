#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Lapyrinthe
main.py - Code de base pour lancer l'UI
lab.py - Classes pour la création du Labyrinthe
player.py - Classes pour le joueur
constants.py - Fichier de constantes
img/*.png - cases du Labyrinthe
"""

import sys
import pygame as pg
import constants as PC

if getattr(sys, 'frozen', False):
    basedir = sys._MEIPASS
else:
    basedir = "."


class Play():
    def __init__(self, lvl):
        if lvl == 'easy':
            self.COTE_X = 15  # nombre de cellules en X
            self.COTE_Y = 9  # nombre de cellules en Y
            self.DIFFICULTY = lvl
        elif lvl == 'medium':
            self.COTE_X = 25  # nombre de cellules en X
            self.COTE_Y = 18  # nombre de cellules en Y
            self.DIFFICULTY = lvl
        elif lvl == 'hard':
            self.COTE_X = 32  # nombre de cellules en X
            self.COTE_Y = 23  # nombre de cellules en Y
            self.DIFFICULTY = lvl

        self.HUD = pg.image.load(
            "{}/img/hud.png".format(basedir, lvl)
        )
        self.BGD = pg.image.load(
            "{}/img/bg.png".format(basedir, lvl)
        )
        self.WIN = pg.image.load(
            "{}/img/win.png".format(basedir, lvl)
        ).convert_alpha()

        self.SCREEN = pg.display.set_mode(
            (self.COTE_X * PC.CELL_X, (self.COTE_Y + 1) * PC.CELL_Y)
        )

        self.HUD_COORD = (0, self.COTE_Y * PC.CELL_Y)
        self.HUD_TIME = (0, PC.CELL_Y * self.COTE_Y)

        self.FLAG = pg.image.load(
            "{}/img/items/flag.png".format(basedir)
        ).convert_alpha()

        self.CARROT = pg.image.load(
            "{}/img/items/carrot.png".format(basedir)
        ).convert_alpha()
        self.CARROT_GOT = 0
        self.CARROT_COORD = (
            PC.CELL_X * (self.COTE_X - 1),
            PC.CELL_Y * (self.COTE_Y - 1)
        )
        self.CARROT_HUD = (
            PC.CELL_X * (self.COTE_X - 3),
            PC.CELL_Y * self.COTE_Y
        )

        self.RADIS = pg.image.load(
            "{}/img/items/radis.png".format(basedir)
        ).convert_alpha()
        self.RADIS_GOT = 0
        self.RADIS_COORD = (PC.CELL_X * (self.COTE_X - 1), 0)
        self.RADIS_HUD = (
            PC.CELL_X * (self.COTE_X - 2),
            PC.CELL_Y * self.COTE_Y
        )

        self.SALAD = pg.image.load(
            "{}/img/items/salad.png".format(basedir)
        ).convert_alpha()
        self.SALAD_GOT = 0
        self.SALAD_COORD = (0, PC.CELL_Y * (self.COTE_Y - 1))
        self.SALAD_HUD = (
            PC.CELL_X * (self.COTE_X - 1),
            PC.CELL_Y * self.COTE_Y
        )
