#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Lapyrinthe
main.py - Code de base pour lancer l'UI
lab.py - Classes pour la création du Labyrinthe
player.py - Classes pour le joueur
constants.py - Fichier de constantes
img/*.png - cases du Labyrinthe
"""

import pygame as pg
import datetime
import lab
import player
import play
import constants as PC
import sys
import json

if getattr(sys, 'frozen', False):
    basedir = sys._MEIPASS
else:
    basedir = "."

pg.init()
pg.display.set_caption("Lapyrinthe")
pg.display.set_icon(PC.ICONE)

pg.mixer.music.play()
pg.mixer.music.set_volume(0.5)

CONTINUER = 1
while CONTINUER:
    pg.time.Clock().tick(60)
    MENU = 1
    GAME = 0
    WIN = 0
    FONT = pg.font.Font("{}/img/NotoMono-Regular.ttf".format(basedir), 20)

    # Menu du jeu
    while MENU:
        splash = PC.Splash()
        DISPLAY = splash.FENETRE
        for event in pg.event.get():
            if (
                event.type == pg.QUIT
                or event.type == pg.KEYDOWN
                and event.key == pg.K_ESCAPE
            ):
                CONTINUER = 0
                MENU = 0
                GAME = 0
                WIN = 0
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_F1:
                    MENU = 0
                    GAME = 1
                    WIN = 0
                    DIF = 'easy'
                if event.key == pg.K_F2:
                    MENU = 0
                    GAME = 1
                    WIN = 0
                    DIF = 'medium'
                if event.key == pg.K_F3:
                    MENU = 0
                    GAME = 1
                    WIN = 0
                    DIF = 'hard'
        DISPLAY.blit(splash.ACCUEIL, (0, 0))
        pg.display.flip()

    # Initialisation à chaque nouvelle partie
    if GAME == 1:
        START = datetime.datetime.now()
        TIME = 0
        PL = play.Play(DIF)
        LABY = lab.Laby(PL)
        LABY.make_lab(PL, LABY.cell_list)
        LABY.display(PL, LABY.cell_list)
        PLAYER = player.Perso(LABY.cell_list)

    # Jeu
    while GAME:
        pg.time.Clock().tick(60)
        DISPLAY = PL.SCREEN
        TIME = datetime.datetime.now() - START
        CHRONO = FONT.render(str(TIME.seconds), 1, PC.WHITE)
        for event in pg.event.get():
            if (event.type == pg.QUIT):
                CONTINUER = 0
                MENU = 0
                GAME = 0
                WIN = 0
            if (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                MENU = 1
                GAME = 0
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP:
                    PLAYER.move(PL, "up")
                if event.key == pg.K_DOWN:
                    PLAYER.move(PL, "down")
                if event.key == pg.K_LEFT:
                    PLAYER.move(PL, "left")
                if event.key == pg.K_RIGHT:
                    PLAYER.move(PL, "right")
        PLAYER.collect(PL)
        DISPLAY.blit(PL.BGD, (0, 0))
        DISPLAY.blit(PL.HUD, PL.HUD_COORD)
        PL.SCREEN.blit(CHRONO, PL.HUD_TIME)
        LABY.display(PL, LABY.cell_list)
        LABY.display_hud(PL)
        DISPLAY.blit(PLAYER.perso, PLAYER.perso_pos)
        pg.display.flip()

        if (
            PL.CARROT_GOT == 1
            and PL.RADIS_GOT == 1
            and PL.SALAD_GOT == 1
        ):
            GAME = 0
            MENU = 0
            WIN = 1

    while WIN:
        DISPLAY = PL.SCREEN
        SIZE_X, SIZE_Y = pg.display.get_surface().get_size()
        for event in pg.event.get():
            if (event.type == pg.QUIT):
                CONTINUER = 0
                MENU = 0
                GAME = 0
                WIN = 0
            if (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                MENU = 1
                GAME = 0
                WIN = 0
                PL.CARROT_GOT = 0
                PL.RADIS_GOT = 0
                PL.SALAD_GOT = 0

        DISPLAY.blit(PL.BGD, (0, 0))
        DISPLAY.blit(PL.WIN, (0, 0))
        try:
            PC.SCORES[PL.DIFFICULTY]
        except IndexError:
            WIN_TXT = FONT.render("First Score:", 1, PC.WHITE)
            PC.SCORES.append(TIME.seconds)
        else:
            if int(TIME.seconds) <= PC.SCORES[PL.DIFFICULTY]:
                WIN_TXT = FONT.render("NEW HIGH SCORE:", 1, PC.WHITE)
                DISPLAY.blit(
                    WIN_TXT,
                    (SIZE_X / 2 - WIN_TXT.get_size()[0]/2, SIZE_Y * 1/5)
                )
                DISPLAY.blit(
                    CHRONO,
                    (SIZE_X / 2 - CHRONO.get_size()[0]/2, SIZE_Y * 2/5))
                PC.SCORES[PL.DIFFICULTY] = int(TIME.seconds)
                json.dump(PC.SCORES, open('scores.dat', 'w'))
            else:
                WIN_TXT = FONT.render("Time:", 1, PC.WHITE)
                BEST = "Best : {}".format(str(PC.SCORES[PL.DIFFICULTY]))
                BST_TXT = FONT.render(BEST, 1, PC.WHITE)
                DISPLAY.blit(
                    WIN_TXT,
                    (SIZE_X / 2 - WIN_TXT.get_size()[0]/2, SIZE_Y * 1/5)
                )
                DISPLAY.blit(
                    CHRONO,
                    (SIZE_X / 2 - CHRONO.get_size()[0]/2, SIZE_Y * 2/5))
                DISPLAY.blit(
                    BST_TXT,
                    (SIZE_X / 2 - BST_TXT.get_size()[0]/2, SIZE_Y * 3/5))

        pg.display.flip()

pg.quit()
